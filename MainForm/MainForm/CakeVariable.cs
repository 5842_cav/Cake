﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cake
{
    /// <summary>
    ///     Перечисления возможных типов 
    /// </summary>
    public enum CakeVariable
    {
        Resheight,
        Resradius,
        Resradec,
        Resnumd,
        Rescandlerad
    };
}
