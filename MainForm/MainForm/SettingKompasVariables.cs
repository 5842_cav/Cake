﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kompas6API5;
using Kompas6Constants3D;

namespace Kompas
{
    /// <summary>
    ///     Основные функции подключения через API к "КОМПАС-3D"
    /// </summary>
    class SettingKompasVariables
    {
        public ksPart Part;
        public ksEntity EntitySketch;
        public ksSketchDefinition SketchDef;
        public ksEntity BasePlane;
        public ksDocument2D SketchEdit;
        public ksEntity EntityExtr;
        public ksBaseExtrusionDefinition ExtrusionDef;
        
        /// <summary>
        /// Подключение интерфейсов для выполнения основного функционала 
        /// </summary>
        /// <param name="_kompas">Исходный объект</param>
        public SettingKompasVariables(KompasObject _kompas)
        {
            // Создание документа
            var doc = (ksDocument3D)_kompas.Document3D();
            doc.Create();
            doc = (ksDocument3D)_kompas.ActiveDocument3D();

            Part = (ksPart)doc.GetPart((short)Part_Type.pTop_Part);
            // Создание нового эскиза
            EntitySketch = (ksEntity)Part.NewEntity((short)Obj3dType.o3d_sketch);
            // интерфейс свойств эскиза
            SketchDef = (ksSketchDefinition)EntitySketch.GetDefinition();
            // получение интерфейса базовой плоскости
            BasePlane = (ksEntity)Part.GetDefaultEntity((short)Obj3dType.o3d_planeXOY);
            // интерфейс редактора эскиза
            SketchEdit = (ksDocument2D)SketchDef.BeginEdit();
            // выдавливание
            EntityExtr = (ksEntity)Part.NewEntity((short)Obj3dType.o3d_baseExtrusion);
            // интерфейс свойств базовой операции выдавливания
            ExtrusionDef = (ksBaseExtrusionDefinition)EntityExtr.GetDefinition();
        }
    }
}
