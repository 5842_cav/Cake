using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cake
{
    /// <summary>
	///     ����� ���������� �����
	/// </summary>
    public class CakeParameters
    {
        int _height;
        int _radius;
        int _radec;
        int _numd;
        int _candlerad;

       
        /// <summary>
        ///     ����������� ��������
        /// </summary>
        /// <param name="type">��� ��������</param>
        /// <returns> �������� ����</returns>
        public int GetVariable(CakeVariable type)
        {
            switch (type)
            {
                case CakeVariable.Resheight: return _height;
                case CakeVariable.Resradius: return _radius;
                case CakeVariable.Resradec: return _radec;
                case CakeVariable.Resnumd: return _numd;
                case CakeVariable.Rescandlerad: return _candlerad;
                default:
                    return -1;
            }
        }

        /// <summary>
        ///     �������� �� ��������� � �������� ��������
        /// </summary>
        /// <param name="value">����������� ��������</param>
        /// <param name="type">��� ��������</param>
        public bool IsInBounds(int value, CakeVariable type)
        {
            int min = 0; int max = 0;
            switch (type)
            {
                case CakeVariable.Resheight:
                        max = 200;
                        min = 10;
                        break;
                case CakeVariable.Resradius:
                        max = 275;
                        min = 90;
                        break;
                case CakeVariable.Resradec:
                        min = 50;
                        max = (int)(0.8 * _radius);
                        break;
                case CakeVariable.Resnumd:
                        max = 8;
                        min = 4;
                        break;
                case CakeVariable.Rescandlerad:
                        max = 10;
                        min = 1;
                        break;
            default:
                return false;
            }
            if ((value >= min) && (value <= max))
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        ///     ��������� ��������
        /// </summary>
        /// <param name="value">����������� ��������</param>
        /// <param name="type">��� ��������</param>
        public void SetVariable(int value, CakeVariable type)
        {
            switch (type)
            {
                case CakeVariable.Resheight:
                    _height = value;
                    break;
                case CakeVariable.Resradius:
                    _radius = value;
                    break;
                case CakeVariable.Resradec:
                    _radec = value;
                    break;
                case CakeVariable.Resnumd:
                    _numd = value;
                    break;
                case CakeVariable.Rescandlerad:
                    _candlerad = value;
                    break;
            }
        }
    }
}

