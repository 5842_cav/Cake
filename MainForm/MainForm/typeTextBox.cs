﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cake
{
    /// <summary>
    /// Структура для взаимодействия с текстбоксами
    /// </summary>
    public struct TypeTextBox
    {
        /// <summary>
        ///     Поле ввода
        /// </summary>
        public string ActiveBox { get; set; }

        /// <summary>
        ///     Определенный параметр
        /// </summary>
        public CakeVariable Type { get; set; }

        /// <summary>
        ///     Сообщение об ошибке
        /// </summary>
        public string MessageError { get; set; }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="newActiveBox">Активное поле ввода</param>
        /// <param name="newType">Активный параметр</param>
        /// <param name="newMessageError">Подходящее сообщение об ошибке</param>
        public TypeTextBox(string newActiveBox, 
                           CakeVariable newType, 
                           string newMessageError)
        {
            ActiveBox = newActiveBox;
            Type = newType;
            MessageError = newMessageError;
        }
    }
}
