using System;
using Kompas6API5;
using System.Windows.Forms;
using Kompas6Constants3D;
using Cake;

namespace Kompas
{
    /// <summary>
	///     ������������ ���������� � "������-3D"
	/// </summary>
    public class KompasWrapper
    {
        /// <summary>
        ///     ������ � ������������ �����������
        /// </summary>
        private KompasObject _kompas;
        
        /// <summary>
        ///     ����������� ���������� ��� ��������� �������    
        /// </summary>
        /// <param name="kompas">�������� ������</param>
        public KompasWrapper(KompasObject kompas)
        {
            _kompas = kompas;
        }

        /// <summary>
        ///     ������ ��������� "������-3D"
        /// </summary>
        public void LoadKompas()
        {
            try
            { 
                if (_kompas == null)
                {
                    //�������� �� ������������� ������-3D �� ����������
                    Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
                    _kompas = (KompasObject)Activator.CreateInstance(t);
                }
                else
                {
                    _kompas.Visible = true;
                    //������������� API
                    _kompas.ActivateControllerAPI();
                }
            }
            catch 
            {
                Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
                _kompas = (KompasObject)Activator.CreateInstance(t);
                _kompas.Visible = true;
                _kompas.ActivateControllerAPI();
            }
        }
        
        /// <summary>
        ///     �������� ��������� "������-3D"
        /// </summary>
        public void UnloadKompas()
        {
            try
            {
                _kompas.Quit();
                _kompas = null;
            }
            catch
            {
                _kompas = null;
            }
        }
  
        /// <summary>
        ///     �������� �������� ���������� � �� ������������
        /// </summary>
        /// <param name="vars">������ ����������</param>
        /// <param name="height">������ ����� </param>
        /// <param name="radius">������ �����</param>
        private void CreateCircle(SettingKompasVariables vars,
                                  int height,
                                  int radius)

        {
            if (vars.EntitySketch != null)
            {

                // ��������� ������� ������
                if (vars.SketchDef != null)
                {
                    // ��������� ������� ��������� ��� ������
                    vars.SketchDef.SetPlane(vars.BasePlane);
                    vars.EntitySketch.Create();

                    // ��������� ��������� ������
                    vars.SketchEdit = (ksDocument2D)vars.SketchDef.BeginEdit();

                    vars.SketchEdit.ksCircle(0, 0, radius, 1);
                    // ���������� �������������� ������
                    vars.SketchDef.EndEdit();

                    // ������������
                    if (vars.EntityExtr != null)
                    {
                        var entityColor = (ksColorParam)vars.EntityExtr.ColorParam();
                        // � ������3D ������������ ������ BGR ��� ����������� ����� 
                        entityColor.color = 0x13458B;
                        // ��������� ������� �������� ������������
                        if (vars.ExtrusionDef != null)
                        {
                            vars.ExtrusionDef.directionType = (short)Direction_Type.dtNormal;
                            // ����������� ������������
                            vars.ExtrusionDef.SetSideParam(true,
                                (short)End_Type.etBlind, height);
                            // ����� �������� ������������
                            vars.ExtrusionDef.SetSketch(vars.EntitySketch);
                            vars.EntityExtr.Create(); // ������� ��������
                            var fillet = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_fillet);
                            var filletColor = (ksColorParam)fillet.ColorParam();
                            filletColor.color = 0x13458B;
                            var filletDef = (ksFilletDefinition)fillet.GetDefinition();
                            if (filletDef != null)
                            {
                                filletDef.radius = height / 10;
                                // o���������� ������� ���� �����
                                var edgeArray = (ksEntityCollection)vars.Part.EntityCollection((short)Obj3dType.o3d_edge);
                                // ����������� ����� �� �����
                                edgeArray.SelectByPoint(radius, 0, 0);
                                var filletArray = (ksEntityCollection)filletDef.array();
                                filletArray.Clear();
                                filletArray.Add(edgeArray.First());
                                fillet.Create();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     �������� �����
        /// </summary>
        /// <param name="vars">������ ����������</param>
        /// <param name="candlerad">������ �����</param>
        /// <param name="radius">������ �����</param>
        private void CreateCandle(SettingKompasVariables vars,
                                  int candlerad,
                                  int radius)
        {
            // �������� ������ ������
            vars.EntitySketch = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_sketch);
            if (vars.EntitySketch != null)
            {
                // ��������� ������� ������
                vars.SketchDef = (ksSketchDefinition)vars.EntitySketch.GetDefinition();
                if (vars.SketchDef != null)
                {
                    vars.SketchDef.SetPlane(vars.BasePlane);
                    vars.EntitySketch.Create();
                    // ��������� ��������� ������
                    vars.SketchEdit = (ksDocument2D)vars.SketchDef.BeginEdit();
                    vars.SketchEdit.ksCircle(0, 0, candlerad, 1);
                    vars.SketchDef.EndEdit();

                    // �������� ������������
                    vars.EntityExtr = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_baseExtrusion);
                    if (vars.EntityExtr != null)
                    {
                        var entityColor = (ksColorParam)vars.EntityExtr.ColorParam();
                        entityColor.color = 0xCDFAFF;
                        vars.ExtrusionDef = (ksBaseExtrusionDefinition)vars.EntityExtr.GetDefinition();

                        if (vars.ExtrusionDef != null)
                        {
                            int heightcandle = radius / 2;
                            vars.ExtrusionDef.directionType = (short)Direction_Type.dtReverse;
                            vars.ExtrusionDef.SetSideParam(false,
                                                     (short)End_Type.etBlind, heightcandle);

                            vars.ExtrusionDef.SetSketch(vars.EntitySketch);
                            vars.EntityExtr.Create(); // �������� �������� ������������

                        }
                    }
                }
            }
        }

        /// <summary>
        ///     �������� ��������� �� ��������������� �����
        /// </summary>
        /// <param name="vars">������ ����������</param>
        /// <param name="radec">������ ������������ ���������</param>
        /// <param name="numd">����������� ��������� �� �����</param>
        private void CreateDecorations(SettingKompasVariables vars,
                                       int radec,
                                       int numd)
        {
            // ������� ����� �����
            vars.EntitySketch = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_sketch);
            if (vars.EntitySketch != null)
            {
                // ��������� ������� ������
                vars.SketchDef = (ksSketchDefinition)vars.EntitySketch.GetDefinition();
                if (vars.SketchDef != null)
                {
                    // �������� ��������� � ���������� ���������� ������������ ��������� XOY
                    vars.BasePlane = (ksEntity)vars.Part.GetDefaultEntity((short)Obj3dType.o3d_planeXOY);
                    var copyPlane = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_planeOffset);
                    var copyPlaneDef = (ksPlaneOffsetDefinition)copyPlane.GetDefinition();
                    copyPlaneDef.direction = false;
                    copyPlaneDef.offset = 25;
                    copyPlaneDef.SetPlane(vars.BasePlane);
                    copyPlane.Create();

                    vars.SketchDef.SetPlane(copyPlane);
                    vars.EntitySketch.Create();

                    // ��������� ��������� ������
                    vars.SketchEdit = (ksDocument2D)vars.SketchDef.BeginEdit();

                    // �������� ��� ���������
                    //(x, y, radius, 1), ��� � - ������ ������������ ���������
                    vars.SketchEdit.ksCircle(radec, 0, 20, 1);
                    vars.SketchDef.EndEdit();

                    // ������������ 
                    vars.EntityExtr = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_baseExtrusion);
                    if (vars.EntityExtr != null)
                    {
                        var entityColor = (ksColorParam)vars.EntityExtr.ColorParam();
                        entityColor.color = 0x000080;
                        vars.ExtrusionDef = (ksBaseExtrusionDefinition)vars.EntityExtr.GetDefinition();

                        if (vars.ExtrusionDef != null)
                        {
                            vars.ExtrusionDef.directionType = (short)Direction_Type.dtNormal;
                            vars.ExtrusionDef.SetSideParam(true, (short)End_Type.etBlind, 25);
                            vars.ExtrusionDef.SetSketch(vars.EntitySketch);
                            vars.EntityExtr.Create();
                            // ����������
                            var fillet = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_fillet);
                            var filletColor = (ksColorParam)fillet.ColorParam();
                            filletColor.color = 0x000080;
                            var filletDef = (ksFilletDefinition)fillet.GetDefinition();
                            if (filletDef != null)
                            {
                                int i = +1;
                                filletDef.radius = 25 * 3 / 5;
                                // o���������� ������� ���� �����
                                var edgeArray = (ksEntityCollection)vars.Part.EntityCollection((short)Obj3dType.o3d_edge);
                                // ����������� ����� �� �����
                                edgeArray.SelectByPoint((radec - 20) * Math.Cos(i / numd * 2 * Math.PI), (radec - 20) * Math.Sin(i / numd * 2 * Math.PI), -25);
                                var filletArray = (ksEntityCollection)filletDef.array();
                                filletArray.Clear();
                                filletArray.Add(edgeArray.First());
                                fillet.Create();
                            }
                            
                            //�������� ����������� �� ��������������� �����
                            ksEntity CircCopyEntity = (ksEntity)vars.Part.NewEntity((short)Obj3dType.o3d_circularCopy);
                            if (CircCopyEntity != null)
                            {
                                entityColor = (ksColorParam)CircCopyEntity.ColorParam();
                                entityColor.color = 0x000080;
                                ksCircularCopyDefinition CircCopyDef = (ksCircularCopyDefinition)CircCopyEntity.GetDefinition();
                                if (CircCopyDef != null)
                                {
                                    CircCopyDef.SetAxis(false);
                                    CircCopyDef.SetCopyParamAlongDir(numd, 360.0, true, false);
                                    ksEntity baseAxisOZ = (ksEntity)vars.Part.GetDefaultEntity((short)Obj3dType.o3d_axisOZ);
                                    CircCopyDef.SetAxis(baseAxisOZ);
                                    ksEntityCollection entCol = (ksEntityCollection)CircCopyDef.GetOperationArray();
                                    if (entCol != null)
                                    {
                                        entCol.Add(vars.ExtrusionDef);
                                        entCol.Add(filletDef);
                                    }
                                    CircCopyEntity.Create();
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     ���������� ������
        /// </summary>
        /// <param name="cakeParameters">�������� ���� ��� ����� ����������</param>
        public void CreateDetail(CakeParameters cakeParameters)
        {
            try
            {
                //�������� ������ ����������
                SettingKompasVariables kompasVars = new SettingKompasVariables(_kompas);
                if (kompasVars.Part != null)
                {
                    CreateCircle(kompasVars, cakeParameters.GetVariable(CakeVariable.Resheight), 
                                             cakeParameters.GetVariable(CakeVariable.Resradius));
                    CreateCandle(kompasVars, cakeParameters.GetVariable(CakeVariable.Rescandlerad),
                                             cakeParameters.GetVariable(CakeVariable.Resradius));
                    CreateDecorations(kompasVars, cakeParameters.GetVariable(CakeVariable.Resradec),
                                             cakeParameters.GetVariable(CakeVariable.Resnumd));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: \n" + ex, "������");
            }
        }

		public KompasWrapper()
        {

		}               
    }
}
