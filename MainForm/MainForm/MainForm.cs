﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Kompas6API5;
using Kompas;
using Cake;

namespace MainForm
{
     public partial class MainForm : Form
     {
        /// <summary>
        ///     Объект с подключенным интерфейсом
        /// </summary>
        private KompasObject _kompas;

        /// <summary>
        ///     Объект с применяемым функционалом
        /// </summary>
        private KompasWrapper _kompasWrapp;

        private CakeParameters _cake = new CakeParameters();
        /// <summary>
        ///     Задание/Вывод определонного парамета
        /// </summary>
        public CakeParameters Cake
        {
            get
            {
                return _cake;
            }
            set
            {
                Cake = _cake;
            }
        }

        /// <summary>
        ///     Конвертор строки в число
        /// </summary>
        private int TextToInt(string text)
        {
            int x;
            if (int.TryParse(text, out x))
            {
                return x;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        ///     Обозначение интрефейса для исходного объекта
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
             _kompasWrapp = new KompasWrapper(_kompas);
        }

        /// <summary>
        ///     Обработчик действий для кнопки "Построить"
        /// </summary>
        /// <param name="sender">Поля текстбоксов</param>
        /// <param name="e">Фон тестбоксов</param>
        private void Button_Click(object sender, EventArgs e)
        {
            if (
                    (textBox1.BackColor == Color.White) &&
                    (textBox2.BackColor == Color.White) &&
                    (textBox3.BackColor == Color.White) &&
                    (textBox4.BackColor == Color.White) &&
                    (textBox5.BackColor == Color.White)
                )
            {
                label6.Visible = true;
                label6.Text = "";
                _kompasWrapp.LoadKompas();     
                _kompasWrapp.CreateDetail(Cake);
            }
            else
            {
                label6.Visible = true;
                label6.Text = "Проверьте правильнсоть параметров!";
                label6.ForeColor = Color.Red;
            }
        }
        
        /// <summary>
        ///     Обработчик действий для текстбоксов
        /// </summary>
        /// <param name="sender">Поля текстбоксов</param>
        /// <param name="e">Фон тестбоксов</param>
        private void TextChanged(object sender, EventArgs e)
        {
            var active = (TextBox)sender;
            var errorFlag = false;
            Console.WriteLine(active);
            TypeTextBox[] fieldsForTextBox = new TypeTextBox[5];
            fieldsForTextBox[0] = new TypeTextBox("textBox1", 
                                                  CakeVariable.Resradius, 
                                                  "Радиус торта: 90 - 275 мм");
            fieldsForTextBox[1] = new TypeTextBox("textBox2", 
                                                  CakeVariable.Resheight, 
                                                  "Высота торта: 10 - 200 мм");
            fieldsForTextBox[2] = new TypeTextBox(
                "textBox3",
                CakeVariable.Resradec,
                String.Format(
                    "Радиус украшений: 50 - {0} мм",
                    (int)(0.8 * Cake.GetVariable(CakeVariable.Resradius))));
            fieldsForTextBox[3] = new TypeTextBox("textBox4", 
                                                  CakeVariable.Resnumd, 
                                                  "Кол-во украшений: 4 - 8");
            fieldsForTextBox[4] = new TypeTextBox("textBox5", 
                                                  CakeVariable.Rescandlerad, 
                                                  "Ширина свечи: 1 - 10 мм");

            foreach (TypeTextBox unitOfFieldForTextBox in fieldsForTextBox)
            {
                if (unitOfFieldForTextBox.ActiveBox == active.Name)
                {
                        if (!Cake.IsInBounds(TextToInt(active.Text), 
                                             unitOfFieldForTextBox.Type))
                        {
                            errorFlag = true;
                            label6.Text = unitOfFieldForTextBox.MessageError;
                        }
                        else
                        {
                            errorFlag = false;
                            Cake.SetVariable(TextToInt(active.Text), 
                                             unitOfFieldForTextBox.Type);
                        }
                }
                if (active.Name == "textBox1")
                {
                    textBox3.Enabled = !errorFlag;
                }
                if (errorFlag == true)
                {
                    label6.Visible = true;
                    label6.ForeColor = Color.Red;
                    active.BackColor = Color.Red;
                }
                else
                {
                    label6.Visible = false;
                    active.BackColor = Color.White;
                }
            }
        }

        /// <summary>
        ///     Вызов закрытия "Компас-3D" после закрытия плагина
        /// </summary>
        /// <param name="sender">Поля текстбоксов</param>
        /// <param name="e">Фон тестбоксов</param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                _kompasWrapp.UnloadKompas();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: \n" + ex, "Ошибка");
            }
        }
    }
}
