﻿using System;
using MainForm;
using NUnit.Framework;
using Cake;

namespace UnitTests
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        [TestCase(100, CakeVariable.Resheight, true, 
            TestName = "IsInBounds test for Resheight")]
        [TestCase(100, CakeVariable.Resradius, true, 
            TestName = "IsInBounds test for Resradius")]
        [TestCase(60, CakeVariable.Resradec, true, 
            TestName = "IsInBounds test for Resradec")]
        [TestCase(5, CakeVariable.Resnumd, true, 
            TestName = "IsInBounds test for Resnumd")]
        [TestCase(5, CakeVariable.Rescandlerad, true, 
            TestName = "IsInBounds test for Rescandlerad")]
        [TestCase(300, CakeVariable.Resheight, false, 
            TestName = "Not IsInBounds test for Resheight")]
        [TestCase(300, CakeVariable.Resradius, false, 
            TestName = "Not IsInBounds test for Resradius")]
        [TestCase(100, CakeVariable.Resradec, false, 
            TestName = "Not IsInBounds test for Resradec")]
        [TestCase(10, CakeVariable.Resnumd, false, 
            TestName = "Not IsInBounds test for Resnumd")]
        [TestCase(15, CakeVariable.Rescandlerad, false, 
            TestName = "Not IsInBounds test for Rescandlerad")]
        [TestCase(10, CakeVariable.Resheight, true, 
            TestName = "IsInBounds border test for Resheight")]
        [TestCase(90, CakeVariable.Resradius, true, 
            TestName = "IsInBounds border test for Resradius")]
        [TestCase(50, CakeVariable.Resradec, true, 
            TestName = "IsInBounds border test for Resradec")]
        [TestCase(8, CakeVariable.Resnumd, true, 
            TestName = "IsInBounds border test for Resnumd")]
        [TestCase(1, CakeVariable.Rescandlerad, true, 
            TestName = "IsInBounds border test for Rescandlerad")]
        public void Not_IsInBounds_test(int value, CakeVariable type, bool expected)
        {
            var cake = new Cake.CakeParameters();
            if (type == CakeVariable.Resradec)
            {
                cake.SetVariable(100, CakeVariable.Resradius);
            }
            bool actual = cake.IsInBounds(value, type);
            Assert.AreEqual(expected, actual);
        }
        
        [TestCase(50, CakeVariable.Resheight, 
            TestName = "GetVariable test for Resheight")]
        [TestCase(300, CakeVariable.Resradius, 
            TestName = "GetVariable test for Resradius")]
        [TestCase(40, CakeVariable.Resradec, 
            TestName = "GetVariable test for Resradec")]
        [TestCase(57, CakeVariable.Resnumd, 
            TestName = "GetVariable test for Resnumd")]
        [TestCase(35, CakeVariable.Rescandlerad, 
            TestName = "GetVariable test for Rescandlerad")]
        public void GetVariable_test(int value, 
                                     CakeVariable type)
        {
            
            var cake = new CakeParameters();
            cake.SetVariable(value, type);
            Assert.AreEqual(value, cake.GetVariable(type));
        }
    }
}