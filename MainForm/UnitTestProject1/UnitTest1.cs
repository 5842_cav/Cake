﻿using System;
using MainForm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [DataTestMethod]
        [DataRow(100, Cake.CakeVariable.resheight, true)]
        public void TestMethod1()
        {
            var _cake = new Cake.CakeParameters();
            bool _actual = _cake.IsInBounds(100, Cake.CakeVariable.resheight);
            Assert.AreEqual(true, _actual);
        }
    }
}
